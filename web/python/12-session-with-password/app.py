# https://flask.palletsprojects.com/en/3.0.x/quickstart/#sessions

from flask import Flask, session, request, redirect, url_for, render_template
from config import *
from model import *


app = Flask(__name__)

# Set the secret key to some random bytes. Keep this really secret!
app.secret_key = b'_5#y2L"F4Q8z\n\xec]/'

@app.route('/')
def home():
    return render_template("home.html")

@app.route('/about')
def about():
    return render_template("about.html")

@app.route("/listar_pessoas")
def listar_pessoas():
    with db_session:
        # obtém as pessoas
        pessoas = Pessoa.select() 
        return render_template("listar_pessoas.html", pessoas=pessoas)

@app.route("/form_adicionar_pessoa")
def form_adicionar_pessoa():
    return render_template("form_adicionar_pessoa.html")

# FURO DE segurança: tente executar
# http://localhost:5000/adicionar_pessoa?nome=HAHA&email=HOHO&telefone=HIHI
# como corrigir isso?

@app.route("/adicionar_pessoa")
def adicionar_pessoa():
    # obter os parâmetros
    nome = request.args.get("nome")
    email = request.args.get("email")
    telefone = request.args.get("telefone")
    # salvar
    with db_session:
        # criar a pessoa
        p = Pessoa(**request.args)
        # salvar
        commit()
        # encaminhar de volta para a listagem
        return redirect("listar_pessoas") 

@app.route('/login', methods=['GET', 'POST'])
def login():
    if request.method == 'POST':
        # obtém os parâmetros
        login = request.form['username']
        senha = request.form['password']
        # verifica login / senha
        if login == 'John' and senha == '123':
            # guarda na seção
            session['username'] = login
            session['password'] = senha
            # encaminha para a página principal
            return redirect(url_for('home'))
        
    # caso contrário (requisição GET), retorna formulário
    return render_template("form_login.html")

@app.route('/logout')
def logout():
    # remove the username from the session if it's there
    session.pop('username', None)
    return redirect(url_for('home'))