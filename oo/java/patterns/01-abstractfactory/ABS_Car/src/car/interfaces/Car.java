package car.interfaces;

public interface Car {
    void assemble();
}